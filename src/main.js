import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import './quasar'
import { Dark } from 'quasar'
import moment from 'moment'
import Highcharts from 'highcharts'
import firebase from 'firebase/app'
import NoDataToDisplay from 'highcharts/modules/no-data-to-display'
import OfflineExporting from 'highcharts/modules/offline-exporting'
import HighchartsVue from 'highcharts-vue'
import DefaultLayout from '@/views/layouts/DefaultLayout'
import { formatter } from '@/helpers/currencyFormatter'

moment.locale('ru')
moment().format('LLLL')
Dark.set(true)

Vue.component('DefaultLayout', DefaultLayout)

Vue.config.productionTip = false

Vue.prototype.$formatter = formatter
Vue.prototype.$moment = moment

NoDataToDisplay(Highcharts)
OfflineExporting(Highcharts)
Vue.use(HighchartsVue)

const firebaseConfig = {
  apiKey: 'AIzaSyD89uYisAhZSFsgAJ_UnL1GmX_bqt2iAmA',
  authDomain: 'cashapp-9e816-default-rtdb.firebaseio.com',
  databaseURL: 'https://cashapp-9e816-default-rtdb.firebaseio.com/',
  storageBucket: 'cashapp-9e816-default-rtdb.appspot.com'
}

new Vue({
  router,
  store,
  created () {
    firebase.initializeApp(firebaseConfig)
    this.$store.dispatch('fetchApp')
  },
  render: h => h(App)
}).$mount('#app')

const bgColorsArray = [
  'bg-indigo-9',
  'bg-red-13',
  'bg-pink-10',
  'bg-teal-8',
  'bg-light-green',
  'bg-amber-8',
  'bg-deep-orange-6',
  'bg-lime-9'
]

export const getRandomColor = () => {
  return bgColorsArray[Math.floor(Math.random() * bgColorsArray.length)]
}

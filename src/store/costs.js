export default {
  namespaced: true,
  state: {
    categories: [
      {
        label: 'Без категории',
        value: 0,
        icon: 'fas fa-ban'
      },
      {
        label: 'Продукты питания',
        value: 1,
        icon: 'fas fa-shopping-basket'
      },
      {
        label: 'Дом',
        value: 2,
        icon: 'fas fa-home'
      },
      {
        label: 'Одежда и обувь',
        value: 3,
        icon: 'fas fa-tshirt'
      },
      {
        label: 'Транспорт',
        value: 4,
        icon: 'fas fa-taxi'
      },
      {
        label: 'Еда вне дома',
        value: 5,
        icon: 'restaurant'
      },
      {
        label: 'Топливо',
        value: 6,
        icon: 'fas fa-gas-pump'
      }
    ],
    marksList: [
      {
        label: 'Пятерочка',
        value: 0
      },
      {
        label: 'МАВТ',
        value: 1
      }
    ]
  },
  mutations: {},
  actions: {},
  getters: {}
}

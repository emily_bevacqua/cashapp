import Vue from 'vue'
import Vuex from 'vuex'
import costs from './costs'
import firebase from 'firebase/app'
import 'firebase/database'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    costs
  },
  actions: {
    async fetchApp () {
      try {
        const res = await firebase.database().ref().once('value')
        const apps = res.val()
        console.log(apps)
      } catch (e) {
        console.log(e.message())
      }
    }
  }
})

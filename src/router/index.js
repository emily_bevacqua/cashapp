import Vue from 'vue'
import VueRouter from 'vue-router'
const Home = () => import('@/views/pages/Home')

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    icon: 'home',
    meta: {
      layout: 'DefaultLayout'
    }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router

import {
  QLayout,
  QHeader,
  QToolbar,
  QToolbarTitle,
  QBtn,
  QTabs,
  QRouteTab,
  QDrawer,
  QPageContainer,
  QFooter,
  QList
} from 'quasar'

const path = require('path')
const ESLintPlugin = require('eslint-webpack-plugin')
const VueLoaderPlugin = require('vue-loader/lib/plugin')

module.exports = function (ctx) {
  return {
    css: ['quasar.sass'],
    extras: [
      '@quasar/extras/material-icons/material-icons.css',
      '@quasar/extras/fontawesome-v5/fontawesome-v5.css'
    ],
    supportIE: false,
    build: {
      extendWebpack (cfg) {
        cfg.resolve.extensions = ['.js', '.json', '.vue', '.sass', '.css']
        cfg.resolve.alias = {
          '@': path.resolve(__dirname, './src')
        }
        cfg.module.rules.push({
          test: /\.js$/,
          exclude: /node_modules/,
          loader: 'babel-loader'
        })
        cfg.plugins.push(new ESLintPlugin(), new VueLoaderPlugin())
      }
    },
    framework: {
      iconSet: 'fontawesome-v5',
      components: [
        QLayout, QHeader, QToolbar, QToolbarTitle, QBtn, QTabs, QRouteTab, QDrawer, QPageContainer, QFooter, QList
      ]
    }
  }
}
